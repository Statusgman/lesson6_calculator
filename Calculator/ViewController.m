//
//  ViewController.m
//  Calculator
//
//  Created by Alexander Dupree on 17/04/2017.
//  Copyright © 2017 SBTiOSSchool. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UILabel *display;
@property (nonatomic, assign) BOOL userIsInTheMiddleOfTypingANumber;
@property (nonatomic, strong) NSNumberFormatter *numberFormatter;

// сюда нужно добавить свойство в котором мы будем хранить модель
// для модели необходимо сделать ленивую инициализацию, так же как с numberFormatter

// этот код нужно перенести в модель
@property (nonatomic, strong) NSString *pendingOperation;
@property (nonatomic, assign) NSNumber *result;

@end

@implementation ViewController

#pragma mark - Getters/Setters

/**
 NSNumberFormatter - класс для форматирования чисел. С его помощью можно из строки получить NSNumber
 и наоборот из NSNumber получить NSString
 
 Здесь переопределен геттер numberFormatter и в нем реализована ленивая инициализация
 */
- (NSNumberFormatter *)numberFormatter {
    if (_numberFormatter) {
        _numberFormatter = [[NSNumberFormatter alloc] init];
    }
    return _numberFormatter;
}

#pragma mark - Actions

- (IBAction)digitEntered:(UIButton *)sender {
    NSString *displayedText = self.display.text;
    if (!self.userIsInTheMiddleOfTypingANumber) {
        displayedText = @"";
        self.userIsInTheMiddleOfTypingANumber = YES;
    }
    self.display.text = [NSString stringWithFormat:@"%@%@",displayedText,sender.titleLabel.text];
}

- (IBAction)appendOperation:(UIButton *)sender {
    self.userIsInTheMiddleOfTypingANumber = NO;
    
    self.pendingOperation = sender.titleLabel.text;
}

- (IBAction)performOperation:(UIButton *)sender {
    self.pendingOperation = nil;
    
    // этот код нужно перенести в модель
    self.result = [self.numberFormatter numberFromString: self.display.text];
    if ([self.pendingOperation isEqualToString:@"×"]) {
        
    } else if ([self.pendingOperation isEqualToString:@"-"]) {
       
    } else if ([self.pendingOperation isEqualToString:@"+"]) {
        
    }
}

@end
